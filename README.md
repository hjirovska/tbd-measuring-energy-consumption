# TBD Measuring Energy Consumption

Developed by Hana Jirovská and Sára Juhošová for the [Sustainable Software Engineering](https://luiscruz.github.io/course_sustainableSE) course at the TU Delft.

This repository contains the source code for the TBD energy consumption tool, meant to provide a uniform interface across operating systems on machines that use Intel chips.
